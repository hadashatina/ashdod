unit Asdod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Scale250,DbTables, AtmComp, StdCtrls, Buttons, Mask, ToolEdit,IniFiles,ComObj,
  Db, ComCtrls, AdvSearch;

type
  TForm1 = class(TForm)
    Scale1: TScale;
    AtmHComboBox1: TAtmHComboBox;
    Label9: TLabel;
    Klita_Dir: TFilenameEdit;
    Label1: TLabel;
    Bbn_SaveIni: TBitBtn;
    BitBtn1: TBitBtn;
    Btn_Klita: TBitBtn;
    Edt_Sisma: TEdit;
    Label11: TLabel;
    Edt_Name: TEdit;
    Label10: TLabel;
    Query1: TQuery;
    Database_InterFace: TDatabase;
    Progress1: TProgressBar;
    Edt_Month: TEdit;
    Label5: TLabel;
    Edt_Shana: TEdit;
    Label4: TLabel;
    Label6: TLabel;
    Edt_Lak: TEdit;
    Edt_Shem_lak: TEdit;
    SpeedButton3: TSpeedButton;
    Lak_Search: TAtmAdvSearch;
    sql_Lak: TQuery;
    Edt_Chen: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edt_Movil: TEdit;
    procedure AtmHComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Bbn_SaveIniClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Btn_KlitaClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure Edt_LakChange(Sender: TObject);
    Procedure Bedikat_Netunim;
    Procedure BuildRek2;
    Function  FindSugHomer(InStr:Integer):string;
    Function  AddLeadingZero(Hstr : shortstring): Shortstring;
    function  MakeStrFromNum(
                InStr : shortstring;   {string input}
                Len : byte;            {length of output}
                JustifyLeftOrRight:char;{L: left,R:right}
                LeadZero:byte;         { 0:leading 0;1: leading blank}
                Hituh:char            {L : left;R : right}
                        ):shortstring;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  DBAliasName1,DBUser,
  Dbpassword               : String;
  Atmini                   : Tinifile;
  Sw_Takin : Boolean;
  Mone,Mone1,Count,Kav     : Integer;
  Amount,Neto              : Real;
  SugMetan,Nefh,Row        : Integer;
  Taarich_Bitzua,
  FromDay,ToDay,Tuda,Asmacta,
  MaslulName,Newstr,Mishkal,Nepach,
  Pratim,Homer             : String;
  FileHan                  : TextFile;
  Dd,Mm,Yy                 : Word;
  Metan                    : Array[1..2,1..200] Of Integer;  

Implementation

{$R *.DFM}


Procedure TForm1.AtmHComboBox1Change(Sender: TObject);
Begin
    DBAliasName1:=AtmHComboBox1.Items[AtmHComboBox1.Itemindex] ;
end;

procedure TForm1.FormShow(Sender: TObject);
VAr
  MyStringList  :  TstringList;
  I             :  Integer;
Begin
  MyStringList := TStringList.Create;
  Try
  Session.GetAliasNames(MyStringList);
  For  I := 0  to  MyStringList.Count - 1  do
    AtmHComboBox1.Items.Add(MyStringList[I]);
  Finally
    MyStringList.Free;
  End ;

   Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Ashdod.ini');
   Klita_Dir.Text:=Atmini.readString('Main','Mikum','');
   DBAliasName1:=Atmini.readString('Main','Alias','Atm');
   Dbpassword:=Atmini.readString('Main','Password','');
   Edt_Sisma.Text:=Dbpassword;
   DBUser:=Atmini.readString('Main','User','Sa');
   Edt_Name.Text:=DBUser;
   AtmHComboBox1.ItemIndex:=
     AtmHComboBox1.Items.IndexOf(DBAliasName1);
   If DBAliasName1 <>'' Then
   Begin
     Database_InterFace.Close;
     Database_InterFace.AliasName:=DBAliasName1;
     Database_InterFace.Params.Values ['USERNAME']:= DBUser;
     Database_InterFace.Params.Values ['PASSWORD']:= DBPassword;
     Database_InterFace.Open;
   End;
   Edt_Lak.Text:=Atmini.readString('Main', 'Kod_lak','');
   Edt_Movil.Text:=Atmini.readString('Main', 'Kod_Movil','21');
   Atmini.Free;
End;

Procedure TForm1.Bbn_SaveIniClick(Sender: TObject);
Begin
   Atmini:=Tinifile.create(ExtractFilePath(Application.ExeName)+'Ashdod.ini');
   Atmini.WriteString ('Main','Alias',DBAliasName1);
   Atmini.WriteString ('Main','Password',Dbpassword);
   Atmini.WriteString ('Main','User',DBUser);
   Atmini.WriteString ('Main','Mikum',Klita_Dir.Text);
   Atmini.WriteString ('Main', 'File_Name' ,Klita_Dir.Text);
   Atmini.WriteString ('Main', 'Kod_lak'   , Edt_Lak.Text);
   Atmini.WriteString ('Main', 'Kod_Movil'   , Edt_Movil.Text);   
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
    Close;
end;

procedure TForm1.Btn_KlitaClick(Sender: TObject);
var
  Total_Records, Mis_Shura ,Kod_Hon : Integer;
  Hstr       : String;

  Col   :     integer;
  TmpStr,Temp            : String;
  EndFile                : Integer;
  TmpFloatStr            : String;
  Positiv : integer ;

  ClickedOk : Boolean;
  Str_Shana : String;
  Int_Shana : Integer;

  Code_Sug_day_Tmp , Day_Int : Integer;

Begin
  Bedikat_Netunim;   //    ����� ������
  If Not Sw_takin then
     exit;
  Btn_Klita.Enabled:=False;
  Mone:=0; Mone1:=0;
  With Query1 do
    Begin
      Close;
      Sql.Clear;
      Sql.Add ('Select Kod_Tavla,Teur_tavla');
      Sql.Add ('From KodTavla');
      Sql.Add ('Where Sug_tavla=800');
      Open;
      First;
      While Not Eof Do
      Begin
        Inc(Mone1);
        Metan[1,Mone1]:=FieldByName('Kod_Tavla').AsInteger;
        Metan[2,Mone1]:=FieldByName('Teur_tavla').AsInteger;// ��� �����
        Next;
      End;

      Close;
      Sql.Clear;
      Sql.Add ('Select TotalSumFromTnua,Hesbonitdate From Makav');
      Sql.Add ('Where HesbonitNumber='+Edt_Chen.Text);
      Sql.Add ('And YehusYear='+Edt_Shana.Text);
      Sql.Add ('And CodeLakoach='+Edt_Lak.Text);
      Sql.Add ('And HesbonitKind=1');
      Open;
      First;
      If Eof Then
      Begin
        ShowMessage('�������� �� �����');
        CloseFile(FileHan);
        Exit;
      End;
      DecodeDate(FieldByName('Hesbonitdate').AsDatetime,Yy,Mm,Dd);
      Newstr:=MakeStrFromNum(
         FloatToStr(
         Round(FieldByName('TotalSumFromTnua').AsFloat*100)),
         14, 'R', 1,'R');
      Case Length(Newstr) Of
         1 : Newstr:='00'+Newstr;
         2 : Newstr:='0'+Newstr;
      End;
      Newstr:=Copy(Newstr,1,12)+'.'+Copy(Newstr,13,2);

      Write(FileHan,'01',
         MakeStrFromNum(Edt_Movil.Text,
            10, 'R', 1,'R'),
         MakeStrFromNum(Edt_Chen.Text,
            15, 'R', 1,'R'),
         MakeStrFromNum(IntToStr(Yy),
            4, 'R', 0,'R'),
         MakeStrFromNum(IntToStr(Mm),
            2, 'R', 0,'R'),
         MakeStrFromNum(IntToStr(Dd),
            2, 'R', 0,'R'),
         NewStr,
         #13,#10);

      Close;
      Sql.Clear;
      Sql.Add ('Select Atmshib.*,Atmazmn.Asmcta From Atmshib,Atmazmn');
      Sql.Add ('Where LakHesbonit='+Edt_Chen.Text);
      Sql.Add ('And YehusYear='+Edt_Shana.Text);
      Sql.Add ('And YehusMonth='+Edt_Month.Text);
      Sql.Add ('And Lakno1='+Edt_Lak.Text);
      Sql.add ('And Shibazmnno=azmnno');

      Open;
      First;

      Progress1.Position := 0;
      While Not Eof Do
        Begin
          BuildRek2;
          Progress1.StepIt;
          If Progress1.Position > Progress1.Max then
            Progress1.Position := Progress1.Step;
        Next;
      End; // While Not Eof
    End; // With Query1 do
    CloseFile(FileHan);
    ShowMessage('���� ��� ������� �����');
//-----------------------

//  Edit1.Visible:=True;
//  Label2.Visible:=True;
End;

Procedure TForm1.BuildRek2;
Begin

   SugMetan:=Query1.FieldByName('SugMetan').AsInteger;
   Homer:=FindSugHomer(SugMetan);
   Newstr:=MakeStrFromNum(
      FloatToStr(
      Round(Query1.FieldByName('PriceQuntity1').AsFloat*100)),
      14, 'R', 1,'R');
   Case Length(Newstr) Of
      1 : Newstr:='00'+Newstr;
      2 : Newstr:='0'+Newstr;
   End;
   Newstr:=Copy(Newstr,1,12)+'.'+Copy(Newstr,13,2);

   Mishkal:=MakeStrFromNum(' ',15, 'R', 1,'R');            // ����
   If Query1.FieldByName('MISHKAL').AsFloat<>0 Then
   Begin
     Mishkal:=MakeStrFromNum(
        FloatToStr(
        Round(Query1.FieldByName('MISHKAL').AsFloat*100)),
        14, 'R', 1,'R');
     Case Length(Mishkal) Of
        1 : Mishkal:='00'+Mishkal;
        2 : Mishkal:='0'+Mishkal;
     End;
     Mishkal:=Copy(Mishkal,1,12)+'.'+Copy(Mishkal,13,2);
   End;

   Nepach:=MakeStrFromNum(' ',10, 'R', 1,'R');            // ���
   If Query1.FieldByName('Nefh').AsFloat<>0 Then
   Begin
     Nepach:=MakeStrFromNum(
        FloatToStr(
        Round(Query1.FieldByName('Nefh').AsFloat*100)),
        9, 'R', 1,'R');
     Case Length(Nepach) Of
        1 : Nepach:='00'+Nepach;
        2 : Nepach:='0'+Nepach;
     End;
     Nepach:=Copy(Nepach,1,7)+'.'+Copy(Nepach,8,2);
   End;

   DecodeDate(Query1.FieldByName('ShibAzmnDate').AsDatetime,Yy,Mm,Dd);

   Write(FileHan,'02',
    MakeStrFromNum(
      Query1.FieldByName('Asmcta').Asstring,
        10, 'R', 1,'R'),
      Newstr,
      MakeStrFromNum(
        Homer,2, 'R', 1,'R'),
//    MakeStrFromNum(' ',15, 'R', 1,'R'),            // ����
      Mishkal,
      MakeStrFromNum(
        Query1.FieldByName('Quntity1').Asstring,     // ����
        5, 'R', 0,'R'),
      Nepach,
//    MakeStrFromNum(' ',10, 'R', 1,'R'),            // ���
      MakeStrFromNum(IntToStr(Yy),
        4, 'R', 0,'R'),
      MakeStrFromNum(IntToStr(Mm),
        2, 'R', 0,'R'),
      MakeStrFromNum(IntToStr(Dd),
        2, 'R', 0,'R'),
      MakeStrFromNum(' ',5, 'R', 1,'R'),            // ��� �����
      MakeStrFromNum(
        Query1.FieldByName('Shibrem1').Asstring,    // �����
        40, 'R', 1,'R'),
      MakeStrFromNum(
        Query1.FieldByName('Maslul1').Asstring,
        30, 'R', 1,'R'),
      MakeStrFromNum(
        Query1.FieldByName('Shibrem2').Asstring,    // ���� ������
        30, 'R', 1,'R'),

   #13,#10);
End;

Function TForm1.FindSugHomer(InStr:Integer):string;
Var
I : Integer;
Begin
    Homer:='0';
    For I:=1 To Mone1 Do
    Begin
      If Metan[1,I]=InStr Then
       Homer:=IntToStr(Metan[2,I]);
    End;
    Result:=Homer;
End;


Procedure TForm1.SpeedButton3Click(Sender: TObject);
Begin
  if Lak_Search.Execute then
    Begin
      Edt_Lak.Text:= Lak_Search.ReturnString;
    End; {end if}
End;

procedure TForm1.Edt_LakChange(Sender: TObject);
begin
   With Query1 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Lakoach,Shem_Lakoach From Lakoach');
         Sql.Add ('Where Kod_Lakoach = '+Edt_Lak.Text);
         Open;

         if FieldByName ('Kod_Lakoach').AsString = Edt_Lak.Text then
            begin
              Edt_Shem_Lak.Text := FieldByName ('Shem_Lakoach').AsString;
            end
         else
            Edt_Shem_Lak.Text := '';
       end;

end;

Procedure TForm1.Bedikat_Netunim;
begin
  Sw_Takin := True;

  If (Edt_Shana.Text = '')  or
     (Edt_Month.Text = '') then
     begin
       ShowMessage ('��� ���� �/�� ��� ����');
       Sw_Takin := False;
       Exit;
     end;
  If Edt_Chen.Text = '' Then
  Begin
      ShowMessage ('��� ���� �-� ');
      Sw_Takin := False;
      Exit;
  End;

   AssignFile(FileHan,Klita_dir.Text);
    try
    Rewrite(FileHan);
   Except
   On exception do
     Begin
       showmessage('!!!  ����� ����� ���� ����');
       Sw_Takin := False;
       Exit;
     End;
   End;

End;

function TForm1.MakeStrFromNum(
                InStr : shortstring;   {string input}
                Len : byte;            {length of output}
                JustifyLeftOrRight:char;{L: left,R:right}
                LeadZero:byte;         { 0:leading 0;1: leading blank}
                Hituh:char            {L : left;R : right}
                        ):shortstring;
var ix1 : byte;HStr : shortstring;
    LStr : byte;
begin
  LStr:=length(InStr);
  if Len < LStr then
  begin
    if Hituh = 'L' then
     HStr:=copy(InStr,1,Len);
    if Hituh = 'R' then
     HStr:=copy(InStr,LStr - Len +1,Len);
    InStr:=HStr;
    LStr:=length(InStr);
  end;
  if Len > LStr then
  begin
    HStr:=InStr;
    if JustifyLeftOrRight = 'L' then
      for ix1:=1 to (Len - LStr) do
        Hstr:=HStr+' ';
    if JustifyLeftOrRight = 'R' then
      for ix1:=1 to (Len - LStr) do
        Hstr:=' '+HStr;
    InStr:=HStr;
    LStr:=length(InStr);
  end;
  if LeadZero = 0 then
  begin
     HStr:=AddLeadingZero(InStr);
     InStr:=HStr;
  end;
  Result:=InStr;
end; {MakeStrFromNum}

Function TForm1.AddLeadingZero(Hstr : shortstring): Shortstring;
var
Indx : smallint;
Begin
    for indx:=1 to length(Hstr) Do
       If Hstr[Indx]=' ' then Hstr[Indx]:='0';
    result:=Hstr;
end; {AddLeadingZero}


End.
