object Form1: TForm1
  Left = 91
  Top = 98
  Width = 467
  Height = 309
  BiDiMode = bdRightToLeft
  Caption = '��� ������� �����'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 376
    Top = 17
    Width = 65
    Height = 16
    Caption = '�� ������'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label1: TLabel
    Left = 338
    Top = 227
    Width = 103
    Height = 16
    Caption = '�� ����� ������'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 203
    Top = 45
    Width = 37
    Height = 16
    Caption = '�����'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label10: TLabel
    Left = 378
    Top = 48
    Width = 63
    Height = 16
    Caption = '�� �����'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label5: TLabel
    Left = 382
    Top = 79
    Width = 59
    Height = 16
    Caption = '���� ����'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 389
    Top = 108
    Width = 52
    Height = 16
    Caption = '��� ����'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 390
    Top = 172
    Width = 51
    Height = 16
    Caption = '��� ����'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object SpeedButton3: TSpeedButton
    Left = 137
    Top = 169
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      04000000000080000000120B0000120B00001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD0660060DDDDDDDDDD00FF0DDDDDDDDDDD0FF00505
      0DDDDDD000FFF080DDDDDD088FFFF080DDDDDD0880FFFF080DDDDD0880FF0088
      0DDDDD08880FF08880DDDDD0888880800DDDDDDD000007880DDDDDDDDD077777
      0DDDDDDDDDD00000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
    OnClick = SpeedButton3Click
  end
  object Label2: TLabel
    Left = 392
    Top = 140
    Width = 50
    Height = 16
    Caption = '���� �-�'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 385
    Top = 201
    Width = 57
    Height = 16
    Caption = '��� �����'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object AtmHComboBox1: TAtmHComboBox
    Left = 233
    Top = 14
    Width = 90
    Height = 21
    BiDiMode = bdLeftToRight
    ItemHeight = 13
    ParentBiDiMode = False
    TabOrder = 0
    OnChange = AtmHComboBox1Change
    IsFixed = False
    IsMust = False
    DefaultKind = dkString
    KeyToRunSearch = ksF1
    LabelColorSelect = clActiveCaption
    LabelTextColorSelect = clCaptionText
    Hebrew = True
    StatusBarPanelNum = -1
  end
  object Klita_Dir: TFilenameEdit
    Left = 129
    Top = 224
    Width = 194
    Height = 21
    Filter = 'Any Files|*.*'
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    NumGlyphs = 1
    TabOrder = 9
  end
  object Bbn_SaveIni: TBitBtn
    Left = 10
    Top = 56
    Width = 95
    Height = 25
    Caption = '���� ������'
    TabOrder = 10
    OnClick = Bbn_SaveIniClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
      00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
      00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
      00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
      0003737FFFFFFFFF7F7330099999999900333777777777777733}
    NumGlyphs = 2
  end
  object BitBtn1: TBitBtn
    Left = 10
    Top = 82
    Width = 95
    Height = 25
    Caption = '�����'
    ModalResult = 1
    TabOrder = 11
    OnClick = BitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
      03333377777777777F333301111111110333337F333333337F33330111111111
      0333337F333333337F333301111111110333337F333333337F33330111111111
      0333337F333333337F333301111111110333337F333333337F33330111111111
      0333337F3333333F7F333301111111B10333337F333333737F33330111111111
      0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
      0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
      0333337F377777337F333301111111110333337F333333337F33330111111111
      0333337FFFFFFFFF7F3333000000000003333377777777777333}
    NumGlyphs = 2
  end
  object Btn_Klita: TBitBtn
    Left = 10
    Top = 108
    Width = 95
    Height = 25
    Caption = '���'
    TabOrder = 12
    OnClick = Btn_KlitaClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object Edt_Sisma: TEdit
    Left = 130
    Top = 45
    Width = 47
    Height = 21
    TabOrder = 2
  end
  object Edt_Name: TEdit
    Left = 277
    Top = 45
    Width = 46
    Height = 21
    TabOrder = 1
  end
  object Progress1: TProgressBar
    Left = 128
    Top = 254
    Width = 201
    Height = 16
    Min = 0
    Max = 400
    TabOrder = 13
  end
  object Edt_Month: TEdit
    Left = 277
    Top = 75
    Width = 46
    Height = 21
    TabOrder = 3
  end
  object Edt_Shana: TEdit
    Left = 277
    Top = 104
    Width = 46
    Height = 21
    MaxLength = 4
    TabOrder = 4
  end
  object Edt_Lak: TEdit
    Left = 278
    Top = 168
    Width = 46
    Height = 21
    TabOrder = 6
    OnChange = Edt_LakChange
  end
  object Edt_Shem_lak: TEdit
    Left = 177
    Top = 169
    Width = 88
    Height = 19
    TabStop = False
    Color = clAqua
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 8
  end
  object Edt_Chen: TEdit
    Left = 278
    Top = 136
    Width = 46
    Height = 21
    MaxLength = 4
    TabOrder = 5
  end
  object Edt_Movil: TEdit
    Left = 277
    Top = 196
    Width = 46
    Height = 21
    TabOrder = 7
    OnChange = Edt_LakChange
  end
  object Scale1: TScale
    Active = True
    X_ScreenWidth = 640
    Y_ScreenHeight = 480
    ScaleGrids = True
    ScaleImages = True
    Left = 8
    Top = 8
  end
  object Query1: TQuery
    DatabaseName = 'DBInter_Face'
    Left = 128
    Top = 4
  end
  object Database_InterFace: TDatabase
    DatabaseName = 'DBInter_Face'
    LoginPrompt = False
    SessionName = 'Default'
    Left = 64
    Top = 8
  end
  object Lak_Search: TAtmAdvSearch
    Caption = '����� ����'
    KeyIndex = 0
    SourceDataSet = sql_Lak
    ShowFields.Strings = (
      'Shem_Lakoach'
      'Kod_Lakoach')
    ShowFieldsHeader.Strings = (
      '�� ����'
      '��� ����')
    ShowFieldsDisplayWidth.Strings = (
      '100'
      '100')
    ReturnFieldIndex = 1
    SeparateChar = ','
    IniSection = 'Lak_Search'
    KeepList = False
    EditBiDiMode = bdLeftToRight
    Left = 21
    Top = 237
  end
  object sql_Lak: TQuery
    DatabaseName = 'DBInter_Face'
    SQL.Strings = (
      'Select Kod_Lakoach,Shem_Lakoach '
      'From  Lakoach')
    Left = 74
    Top = 239
  end
end
